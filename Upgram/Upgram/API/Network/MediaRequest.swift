import Foundation
import SwiftyJSON
import Alamofire

class MediasModel {
    
    var mediaUrl:String?
    var isVideo: Bool = false
    
    init (_ mediaUrl:String, isVideo: Bool)
    {
        self.mediaUrl = mediaUrl
        self.isVideo = isVideo
    }
}

struct MediaRequest {
    static func new(link: String, completionHandler: ((_ feedModelOwner: FeedModelOwner?, _ mediaUrl: String?, _ isVideo: Bool, _ medias: [MediasModel]?, _ caption: String?) -> Void)?) {
        if let url = URL(string: link) {
            let url = "https://www.instagram.com/p/" + url.lastPathComponent + "/?__a=1"
            
            Request.new(path: url) { (response, error) in
                
                if let dict = response as? [String: Any]{
                    print("this is the original dict ===>" ,dict)
                    if dict.count == 0{
                        DispatchQueue.main.async {
                            completionHandler?(nil, "", false, nil, nil)
                        }
                        return
                    }
                }
               
                if let dict = response as? [String: Any],
                    let graphql = dict["graphql"] as? [String: Any],
                    let shortcode_media = graphql["shortcode_media"] as? [String: Any] {
                    
                    let dicjson = JSON(dict)
                    
                    print("thisis the firstdata for show pictures===>",dicjson)
                    
                    let is_video = (shortcode_media["is_video"] as? NSNumber)?.boolValue ?? false
                    var mediaUrl: String?
                    var feedModelOwner: FeedModelOwner?
                    var medias = [MediasModel]()
                    if is_video {
                        mediaUrl = shortcode_media["video_url"] as? String
                    } else {
                        mediaUrl = shortcode_media["display_url"] as? String
                    }
                    
                    if let owner = shortcode_media["owner"] as? [String: Any],
                        let id = owner["id"] as? String,
                        let full_name = owner["full_name"] as? String,
                        let username = owner["username"] as? String,
                        let profile_pic_url = owner["profile_pic_url"] as? String {
                        feedModelOwner = FeedModelOwner(full_name: full_name, id: id, profile_pic_url: profile_pic_url, username: username)
                    }
                    
                    
                    let graphql = JSON(dicjson["graphql"].object)
                    let shortcode_mediajson = JSON(graphql["shortcode_media"].object)
                    let captionobject = JSON(shortcode_mediajson["edge_media_to_caption"].object)
                    let edges = captionobject["edges"].arrayObject
                    
                    var caption: String?
                    if let edges = edges{
                        let firstnode = JSON(JSON(edges.first as Any)["node"])["text"].stringValue
                        caption = firstnode
                    }
                                        
                    let children = JSON(shortcode_mediajson["edge_sidecar_to_children"].object)
                    if let edges = children["edges"].arrayObject{
                        if edges.count != 0{
                            medias.removeAll()
                            var num = 0
                            for one in edges{
                                
                                num += 1
                                let oneNode = JSON(one)
                                let node = JSON(oneNode["node"].object)
                                var displayUrl: String
                                let isVideo = node["is_video"].boolValue
                                if isVideo{
                                    displayUrl = node["video_url"].stringValue
                                }else{
                                    displayUrl = node["display_url"].stringValue
                                }
                                
                                medias.append(MediasModel(displayUrl, isVideo: isVideo))
                                
                                if num == edges.count{
                                    DispatchQueue.main.async {
                                        completionHandler?(feedModelOwner, mediaUrl!, is_video, medias, caption)
                                    }
                                }
                            }
                        }else{
                            DispatchQueue.main.async {
                                completionHandler?(feedModelOwner, mediaUrl!, is_video, nil, caption)
                            }
                        }
                    }else{
                        DispatchQueue.main.async {
                            completionHandler?(feedModelOwner, mediaUrl!, is_video, nil, caption)
                        }
                    }
                    
                } else {
                    
                    DispatchQueue.main.async {
                        completionHandler?(nil, nil, false, nil, nil)
                    }
                }
            }
        }
    }
    
    
    static func fetchStory(_ user_pk: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ())  {
        
        let requestUrl = Foundation.URL(string: "https://i.instagram.com/api/v1/feed/user/" + "\(user_pk)" + "/reel_media/")
        var request = URLRequest(url: requestUrl!)
        
        let headers = ["x-ig-capabilities": "3w==", "Accept-Language": "en-GB,en-US;q=0.8,en;q=0.6", "Referer":"https://www.instagram.com/", "authority":"i.instagram.com/", "Accept":"*/*"]
        request.httpMethod = "GET"
    
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 123.1.0.26.115 (iPhone11,8; iOS 13_3; en_US; en-US; scale=2.00; 828x1792; 190542906)", forHTTPHeaderField: "User-Agent")
        //request.setValue("Instagram 9.5.2 (iPhone7,2; iPhone OS 14_0_0; en_US; en-US; scale=2.00; 750x1334) AppleWebKit/420+", forHTTPHeaderField: "User-Agent")
        //request.setValue("ds_user_id=4340950699;sessionid=4340950699%3AXx01RodzAGN2JR%3A20;", forHTTPHeaderField: "Cookie") // for jerilyn
        request.setValue("ds_user_id=41530986735;sessionid=41530986735%3AX4wPIeN4pfLC5b%3A0;", forHTTPHeaderField: "Cookie") // for shiyan
        
        request.allHTTPHeaderFields = headers
        //request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                  case .failure:
                  completion(false, nil)
                  case .success(let data):
                  let dict = JSON(data)
                  //print("thisis the fetchstory====>",dict)

                  let status = dict["status"].stringValue // 0,1,2

                  if status == "ok" { // User authenticate
                      completion(true,dict)
                  }else {
                      completion(false, dict)
                  }
              }
        }
    }
    
    static func findUser(_ userName: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ())  {
        
        let requestUrl = Foundation.URL(string: "https://i.instagram.com/api/v1/users/search?q=" + userName)
        var request = URLRequest(url: requestUrl!)
        
        let headers = ["x-ig-capabilities": "3w==", "Accept-Language": "en-GB,en-US;q=0.8,en;q=0.6", "Referer":"https://www.instagram.com/", "authority":"i.instagram.com/", "Accept":"*/*"]
        request.httpMethod = "GET"
    
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Mozilla/5.0 (iPhone; CPU iPhone OS 13_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 123.1.0.26.115 (iPhone11,8; iOS 13_3; en_US; en-US; scale=2.00; 828x1792; 190542906)", forHTTPHeaderField: "User-Agent")
        //request.setValue("Instagram 9.5.2 (iPhone7,2; iPhone OS 14_0_0; en_US; en-US; scale=2.00; 750x1334) AppleWebKit/420+", forHTTPHeaderField: "User-Agent")
        //request.setValue("ds_user_id=4340950699;sessionid=4340950699%3AXx01RodzAGN2JR%3A20;", forHTTPHeaderField: "Cookie") // for jerilyn
        request.setValue("ds_user_id=41530986735;sessionid=41530986735%3AX4wPIeN4pfLC5b%3A0;", forHTTPHeaderField: "Cookie") // for shiyan
        request.allHTTPHeaderFields = headers
        //request.httpBody = try! JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        Alamofire.request(request).responseJSON { response in
            switch response.result {
                  case .failure:
                  completion(false, nil)
                  case .success(let data):
                  let dict = JSON(data)
                  //print(dict)

                  let status = dict["status"].stringValue // 0,1,2

                  if status == "ok" { // User authenticate
                      completion(true,dict)
                  }else {
                      completion(false, dict)
                  }
              }
        }
    }
    
    private init(){}
}



