import AVFoundation
import AVKit
import Kingfisher
import Toast_Swift
import UIKit
import SwiftyJSON
import Canvas
import DCAnimationKit
import GoogleMobileAds
import KAWebBrowser

final class DownloadController: UIViewController, StoryboardInstantiable, KeyboardHandlerProtocol, PreviewMediaProtocol, PhotoPreviewControllerDelegate {
    
    static var storyboardName: String = DownloadController.identifier
    var textFieldFrame: CGRect = CGRect.zero
    //var userName: String = ""
    var interstitial: GADInterstitial?
    // MARK: - Outlets
    var pasteboard = UIPasteboard.general
    var downloadcase: downloadCase? = downloadCase.general
    @IBOutlet var uiv_container: UIView!
    @IBOutlet var middleView: UIView!
    //@IBOutlet private weak var progressView: UIProgressView!
    //@IBOutlet weak var progressView: LinearProgressView!
    
    @IBOutlet weak var cons_t_total: NSLayoutConstraint!
    @IBOutlet weak var totalView: UIView!
    @IBOutlet weak var cons_edtRight: NSLayoutConstraint!
    @IBOutlet weak var imv_preview: UIImageView!
    @IBOutlet weak var progressView: LinearProgressBar!
    @IBOutlet private weak var urlTextField: UITextField! {
        didSet {
            urlTextField.attributedPlaceholder = NSAttributedString(string: urlTextField.placeholder ?? "",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.appViolet.alpha(0.5)])
        }
    }
    @IBOutlet weak var lbl_postPreview: UILabel!
    @IBOutlet weak var btn_downlaod: UIButton!
    
    @IBOutlet weak var lbl_progress: UILabel!
    @IBOutlet private weak var logoImageView: UIImageView!
    @IBOutlet private weak var playerView: UIView!
    //@IBOutlet private weak var headerView: UIView!
    let screenHeight = UIScreen.main.bounds.height
    @IBOutlet private weak var userPhotoImageView: UIImageView! {
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleImageTapGestureRecognizer))
            userPhotoImageView?.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    //@IBOutlet weak var uiv_firstShowView: UIView!
    //@IBOutlet private weak var textInfoLabel: UILabel!
    //@IBOutlet private weak var profileImageView: UIImageView!
    //@IBOutlet weak var lbl_previewPost: UILabel!
    //@IBOutlet weak var edt_caption: UITextField!
    @IBOutlet weak var uiv_animShare: CSAnimationView!
    @IBOutlet weak var uiv_animEdt: CSAnimationView!
    @IBOutlet weak var lbl_copyPaste: UILabel!
    //@IBOutlet private weak var userProfileButton: UIButton!
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var controlsView: UIView! {
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleControlsTapGestureRecognizer))
            controlsView?.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    @IBOutlet private weak var muteButton: UIButton! {
        didSet {
            muteButton.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
            muteButton.layer.shadowOffset = CGSize(width: 0, height: 2)
            muteButton.layer.shadowOpacity = 1.0
            muteButton.layer.shadowRadius = 0.0
            muteButton.layer.masksToBounds = false
            muteButton.layer.cornerRadius = 4.0
        }
    }
    
    // MARK: - Global vars
    private var playerLayer: AVPlayerLayer?
    private var player: AVPlayer?
    private var isVideo = false
    private var feedModelOwner: FeedModelOwner?
    private var medias: [MediasModel]?
    //private var stories: [MediasModel]?
    private var currentUrl: URL?
    var caption: String = ""
    
    var initialUrl: String? {
        didSet {
            if let initialUrl = initialUrl {
                self.previewMedia(text: initialUrl)
            }
        }
    }
    
    var bannerView: GADBannerView?
    
    func setPreview(_ show: Bool) {
        if show{
            self.imv_preview.isHidden = false
            self.lbl_postPreview.isHidden = false
        }else{
            self.imv_preview.isHidden = true
            self.lbl_postPreview.isHidden = true
        }
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setEdtPlaceholder(urlTextField, placeholderText: "Paste link here", placeColor: UIColor.lightGray, padding: .left(40))
        self.middleView.backgroundColor = .clear
        
        //self.headerView.isHidden = true
        self.playerView.isHidden = true
        //self.lbl_previewPost.isHidden = true
        //self.uiv_firstShowView.isHidden = false
        self.setPreview(true)
        self.lbl_progress.isHidden = true
        self.progressView.isHidden = true
        self.uiv_container.isHidden = true
        //btn_downlaod.isHidden = true
        
        urlTextField.changeClearButton()
        urlTextField.addTarget(self, action: #selector(DownloadController.textFieldDidChange(_:)), for: .editingChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(gotoActive),name:.gotoActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showAd),name:.showAd, object: nil)
        self.lbl_postPreview.text = "PREVIEW\nPOST"
        
        guard bannerView == nil else { return}
        bannerView = addBannerView(toView: view, bannerViewDelegate: self, rootController: self)
        self.setStatusBarAndInitialAdvertisement()
    }
    
    func setStatusBarAndInitialAdvertisement()  {
        //let statusBar: UIView = UIApplication.shared.statusBarUIView!
        
        //statusBar.backgroundColor = UIColor.init(red: 23/255, green: 23/255, blue: 23/255, alpha: 1)

        // Do any additional setup after loading the view.
        UIApplication.shared.isStatusBarHidden=true
        interstitial = createAndLoadInterstitial()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        //interstitial = GADInterstitial(adUnitID: ADMOB_INTERTITIALID)
        //interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/1033173712")
        interstitial = GADInterstitial(adUnitID: CONST.initialID)
        interstitial?.delegate = self
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID as! String]
        interstitial?.load(request)
       
        return interstitial!
    }
    
    @objc func showToasts(){
        DispatchQueue.main.async {
            if self.caption != ""{
                print("this is the first" , self.caption)
                self.pasteboard.string = self.caption
                self.showToastCenter("Download Complete\n  Caption Copied", duration: 1.5)
            }else{
                self.showToastCenter("Download Complete", duration: 1.5)
            }
            self.startAnimShareView()
        }
    }
    
    func startAnimShareView()  {// last action
        /*self.cons_edtRight.constant = 80
        self.uiv_animShare.isHidden = false
        self.uiv_animShare.type = "flash"
        self.uiv_animShare.duration = 1
        self.uiv_animShare.delay = 0.5
        self.uiv_animShare.startCanvasAnimation()*/
        //self.cons_t_total.constant = 0
        self.cons_t_total.constant = -(screenHeight / 20)
        print(-screenHeight  / 20)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
          
          self.view.layoutIfNeeded()
        }, completion: { finished in
          print("Basket doors opened!")
        })
    }
    
    @objc func gotoActive(){
        
        self.cons_t_total.constant = -(screenHeight / 20)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
          
          self.view.layoutIfNeeded()
        }, completion: { finished in
          print("Basket doors opened!")
        })
        
        self.urlTextField.text = ""
        self.downloadcase = downloadCase.general
        if let initialUrl = pasteboard.string {
            if initialUrl.contains("instagram.com/"){
                if initialUrl.contains("stories"){
                    self.pasteboard.string = ""
                    self.downloadcase = downloadCase.story
                    urlTextField?.text = initialUrl
                    self.getstories(String(initialUrl.split(separator: "/")[3]))
                }else{
                    urlTextField?.text = initialUrl
                    self.showPost(initialUrl)
                }
            }else{
                self.hidePost()
            }
        }else{
            self.hidePost()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.hideNavBar()
        self.downloadcase = downloadCase.general
        if let initialUrl = pasteboard.string {
            if initialUrl.contains("instagram.com/"){
                if initialUrl.contains("stories"){
                    self.pasteboard.string = ""
                    self.downloadcase = downloadCase.story
                    urlTextField?.text = initialUrl
                    self.getstories(String(initialUrl.split(separator: "/")[3]))
                    
                }else{
                    urlTextField?.text = initialUrl
                    self.showPost(initialUrl)
                }
            }else{
                self.hidePost()
            }
        }else{
            self.hidePost()
        }
        
        self.lbl_progress.isHidden = true
        self.progressView.isHidden = true
        self.uiv_container.isHidden = true
        startObservingKeyboardChanges()
        player?.play()
        NotificationCenter.default.addObserver(self, selector: #selector(showToasts),name:.showToast, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.navigationBar.topItem?.titleView = logoImageView
        navigationController?.navigationBar.topItem?.titleView?.contentMode = .scaleAspectFit
        navigationController?.navigationBar.topItem?.title = ""
    }
    
    override func viewDidLayoutSubviews() {// other screen came back here screen again.
        super.viewDidLayoutSubviews()
    }
    
    @objc func showAd(){
        
        //bannerView = addBannerView(toView: view, bannerViewDelegate: self, rootController: self)
        self.setStatusBarAndInitialAdvertisement()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        //NotificationCenter.default.removeObserver(self)
    }
    
    func getstories(_ username: String) {
        
        self.medias = [MediasModel]()
        MediaRequest.findUser(username) { (isSuccess, data) in
            if isSuccess{
                let datajson = JSON(data as Any)
                print(datajson)
                let users = datajson["users"].arrayObject
                if let users = users{
                    if users.count != 0{
                        var num = 0
                        var user_pk: String?
                        for one in users{
                            num += 1
                            let jsonone = JSON(one as Any)
                            if jsonone["username"].stringValue == username{
                                user_pk = jsonone["pk"].stringValue
                            }
                            if num == users.count{
                                if let userpk = user_pk{
                                    MediaRequest.fetchStory(userpk) { (isSuccess, data) in
                                        if isSuccess{
                                            let datajson = JSON(data as Any)
                                            //print(datajson)
                                            let items = datajson["items"].arrayObject
                                            if let items = items{
                                                if items.count != 0{
                                                    var itemscount = 0
                                                    self.medias?.removeAll()
                                                    for one in items{
                                                        itemscount += 1
                                                        let videoVersions = JSON(one as Any)["video_versions"].arrayObject
                                                        if let videoversions = videoVersions{
                                                            self.medias?.append(MediasModel(JSON(videoversions.first as Any)["url"].stringValue, isVideo: true))
                                                        }else{
                                                            let imageversions = JSON(JSON(one as Any)["image_versions2"].object)
                                                            let candidates = imageversions["candidates"].arrayObject
                                                            if let candidates = candidates{
                                                                if candidates.count != 0{
                                                                    self.medias?.append(MediasModel(JSON(candidates.first as Any)["url"].stringValue, isVideo: false))
                                                                }
                                                            }
                                                        }
                                                        
                                                        if itemscount == items.count{
                                                            DispatchQueue.main.async {
                                                                if let currentURL = self.medias?.first?.mediaUrl{
                                                                    if let currentUrl = URL(string: currentURL){
                                                                        self.currentUrl = currentUrl
                                                                    }
                                                                }
                                                                self.middleView.backgroundColor = .white
                                                                self.playerView.isHidden = false
                                                                self.lbl_copyPaste.isHidden = true
                                                                self.setPreview(false)
                                                                if let issvideo = self.medias?.first?.isVideo{
                                                                    
                                                                    self.startAnimShareView()
                                                                    
                                                                    if issvideo{
                                                                        if let currentURL = self.currentUrl{
                                                                            self.setUpPlayer(url: currentURL)
                                                                        }
                                                                    }else{
                                                                        if let currentURL = self.currentUrl{
                                                                            self.playerView.sendSubviewToBack((self.controlsView)!)
                                                                            self.playerLayer?.removeFromSuperlayer()
                                                                            self.setImage(with: currentURL.absoluteString, imageView: self.userPhotoImageView)
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    print("no items")
                                                }
                                            }else{
                                                print("items nil")
                                            }
                                        }else{
                                            print("response nil")
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        print("no user matching")
                    }
                }
            }else{
                print("network issue")
            }
        }
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopObservingKeyboardChanges()
        player?.pause()
    }
    
    func setEdtPlaceholder(_ edittextfield : UITextField , placeholderText : String, placeColor : UIColor, padding: UITextField.PaddingSide)  {
        edittextfield.attributedPlaceholder = NSAttributedString(string: placeholderText,
        attributes: [NSAttributedString.Key.foregroundColor: placeColor])
        edittextfield.addPadding(padding)
    }
    
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func showToast(_ message : String, duration: TimeInterval = ToastManager.shared.duration, position: ToastPosition = .bottom) {
        self.view.makeToast(message, duration: duration, position: position)
    }
    
    func showToastCenter(_ message : String, duration: TimeInterval = ToastManager.shared.duration) {
        showToast(message, duration: duration, position: .center)
    }
    
    func showInterstatialAd(){
        if (self.interstitial?.isReady)! {
            self.interstitial?.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
            //self.performSegue(withIdentifier: "sugueToNewsVC", sender: nil)
        }
    }
    
    // MARK: - Buttons actions
    @IBAction func downloadMedia(_ sender: UIButton) {
        
        if let url = currentUrl {
            
            // show adaction
            let num = UserDefault.getInt(key: "count",defaultValue: 0)
            let count = num + 1
            UserDefault.setInt(key: "count", value: count)
            print(num)
            if num % 2 == 0{
                self.showInterstatialAd()
            }
            
            /*self.cons_t_total.constant = -(screenHeight / 20)
            UIView.animate(withDuration: 4.0, delay: 0, usingSpringWithDamping: 1.0, initialSpringVelocity: 30, options: .curveEaseIn, animations: {
                self.cons_t_total.constant = 10
            }, completion: nil)*/
            self.cons_t_total.constant = 0
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
              }, completion: { finished in
                print("Basket doors opened!")
              })
            
            
            
            
            /*DispatchQueue.main.async {
                
                self.uiv_animShare.isHidden = true
                UIView.animate(withDuration: TimeInterval(2), delay: 0,
                               usingSpringWithDamping: 0.9,
                               initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                    self.cons_edtRight.constant = 30
                }, completion: nil)
            }*/
            
            
            
            /*self.uiv_animShare.isHidden = true
            self.uiv_animEdt.type = "pop"
            self.uiv_animEdt.delay = 0
            self.uiv_animEdt.duration = 3
            self.uiv_animEdt.startCanvasAnimation()*/
            
            /*self.uiv_animShare.type = "bounceRight"
            self.uiv_animShare.duration = 3
            self.uiv_animShare.delay = 1*/
            //self.uiv_animShare.startCanvasAnimation()
            
            /*DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                
                self.uiv_animShare.isHidden = true
            }*/
            
            if let medias = medias{
                if medias.count != 0{
                    var totalNum = 0
                    var videosNum = 0
                    var picturesNum = 0
                    for one in medias{
                        totalNum += 1
                        if one.isVideo{
                            videosNum += 1
                        }
                        
                        if totalNum == medias.count{
                            picturesNum = medias.count - videosNum
                        }
                    }
                    let totalTime: Float? = Float(picturesNum) * 0.1 + Float(videosNum) * 1.8
                    var timeInterval: Double = 0
                    if let totalTime = totalTime{
                        timeInterval = Double(totalTime / 100)
                    }
                    self.lbl_progress.isHidden = false
                    self.progressView.isHidden = false
                    self.uiv_container.isHidden = false
                    var runCount = 0
                    var returnedNum: Float = 0
                    if #available(iOS 10.0, *) {
                        Timer.scheduledTimer(withTimeInterval: timeInterval, repeats: true) { timer in
                            
                            runCount += 1
            
                            returnedNum = Float(runCount)
                            self.progressView.progressValue = CGFloat(returnedNum)
                            self.lbl_progress.text = String(format: "%.0f", returnedNum) + "%"
                            //print("thisis photostring",returnedNum)
                            if runCount == 100 {
                                self.progressView.progressValue = 0
                                self.lbl_progress.isHidden = true
                                self.progressView.isHidden = true
                                self.uiv_container.isHidden = true
                                DispatchQueue.main.async {
                                    //self.showToast("Download Complete")
                                    if self.caption != ""{
                                        self.pasteboard.string = self.caption
                                        print("this is the second" , self.caption)
                                        if self.downloadcase == downloadCase.general{
                                            self.showToastCenter("Download Complete\n  Caption Copied", duration: 1.5)
                                        }
                                    }else{
                                        if self.downloadcase == downloadCase.general{
                                            self.showToastCenter("Download Complete", duration: 1.5)
                                        }else{
                                            self.showToastCenter("Story Download Complete", duration: 1.5)
                                        }
                                    }
                                    self.startAnimShareView()
                                }
                                timer.invalidate()
                            }
                        }
                    }
                
                    // starting download
                    dump(medias,name: "this is order")
                    self.downloadMediasWithOrder(medias, index: 0)
                    /*var num = 0
                    for one in medias{
                        num += 1
                        if one.isVideo{
                            progressView.progressValue = 0
                            if let oneurl = one.mediaUrl{
                                DownloadVideo.url(URL(string: oneurl)!) { [weak self] (progress, path) in
                                    if let path = path {
                                        SaveMediaContent.saveVideowithFlag(path)
                                        sender.isEnabled = true
                                    }
                                }
                            }
                        }else{
                            progressView.progressValue = 0
                            if let oneurl = one.mediaUrl{
                                let url = URL(string:oneurl)
                                       KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                                        SaveMediaContent.saveImagewithFlag(image)
                                })
                            }
                        }
                    }*/
                }
            }else{
                self.lbl_progress.isHidden = false
                self.progressView.isHidden = false
                self.uiv_container.isHidden = false
                if isVideo {// the last time here showtoast notification, video download for single action
                    sender.isEnabled = false
                    progressView.progressValue = 0
                    
                    DownloadVideo.url(url) { [weak self] (progress, path) in
                        self?.progressView.progressValue = CGFloat(progress * 100)
                        print(progress)
                        self!.lbl_progress.text = String(format: "%.0f", progress * 100) + "%"
                        
                        if let path = path {
                            SaveMediaContent.saveVideo(path)
                            self?.progressView.progressValue = 0
                            sender.isEnabled = true
                            self!.lbl_progress.isHidden = true
                            self!.progressView.isHidden = true
                            self!.uiv_container.isHidden = true
                            self!.pasteboard.string = self!.caption
                        }
                    }
                }else{ //  image download action for single case
                    KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        
                        var runCount = 0
                        var returnedNum: Float = 0

                        if #available(iOS 10.0, *) {
                            Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { timer in
                                
                                runCount += 1
                
                                returnedNum = Float(runCount)
                                self.progressView.progressValue = CGFloat(returnedNum)
                                self.lbl_progress.text = String(format: "%.0f", returnedNum) + "%"
                                //print(returnedNum)
                                if runCount == 100 {
                                    self.progressView.progressValue = 0
                                    self.lbl_progress.isHidden = true
                                    self.progressView.isHidden = true
                                    self.uiv_container.isHidden = true
                                    DispatchQueue.main.async {
                                       
                                       print("this is the third" , self.caption)
                                       if self.caption != ""{
                                            self.pasteboard.string = self.caption
                                            self.showToastCenter("Download Complete\n  Caption Copied", duration: 1.5)
                                        }else{
                                            self.showToastCenter("Download Complete", duration: 1.5)
                                        }
                                        self.startAnimShareView()
                                    }
                                    timer.invalidate()
                                }
                            }
                        }
                        
                        SaveMediaContent.saveImagewithFlag(image){(isSuccess) in
                            if isSuccess{
                                print("download Image completed for single case")
                            }
                        }
                    })
                }
            }
        }
    }
    
    func downloadMedia(_ media: MediasModel, completionHandler: ((_ downloaded: Bool) -> Void)?) {
        
        if media.isVideo{
            progressView.progressValue = 0
            if let oneurl = media.mediaUrl{
                DownloadVideo.url(URL(string: oneurl)!) { [weak self] (progress, path) in
                    if let path = path {
                        SaveMediaContent.saveVideowithFlag(path){
                            (isdownloaded) in
                            if isdownloaded{
                                completionHandler?(true)
                            }else{
                                completionHandler?(false)
                            }
                        }
                    }
                }
            }
        }else{
            progressView.progressValue = 0
            if let oneurl = media.mediaUrl{
                let url = URL(string:oneurl)
                   KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                    SaveMediaContent.saveImagewithFlag(image){
                        (isDownloaded) in
                        if isDownloaded{
                            completionHandler?(true)
                        }else{
                            completionHandler?(false)
                        }
                    }
                })
            }
        }
    }
    
    func downloadMediasWithOrder(_ medias: [MediasModel], index: Int) {
        var internalIndex = index
        self.downloadMedia(medias[internalIndex]) { (isSuccess) in
            if isSuccess{
                internalIndex += 1
                if internalIndex != medias.count{
                    self.downloadMediasWithOrder(medias, index: internalIndex)
                }else{
                    print("downloaded completed")
                }
            }
        }
    }
    
    func setTimer() -> Float?{
        var runCount = 0
        var returnedNum: Float = 0

        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 0.01, repeats: true) { timer in
                
                runCount += 1
                returnedNum = Float(runCount)
                print(returnedNum)
                if runCount == 100 {
                    timer.invalidate()
                }
            }
            return returnedNum
        } else {
            return nil
        }
    }
    
    @IBAction func playPauseVideo(_ sender: UIButton) {
        sender.isSelected ? player?.pause() : player?.play()
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func muteUnmuteVideo(_ sender: UIButton) {
        player?.isMuted = sender.isSelected
        sender.isSelected = !sender.isSelected
    }
    
    @objc private func handleImageTapGestureRecognizer() {
//        if let url = currentUrl , isVideo == false {
//            previewPhoto(imageUrl: url.absoluteString)
//        }
    }
    
    @objc private func handleControlsTapGestureRecognizer() {
        playButton.isSelected ? player?.pause() : player?.play()
        playButton.isSelected = !playButton.isSelected
        
        UIView.animate(withDuration: 0.3, animations: {
            self.playButton.alpha = CGFloat(fabsf(Float(self.playButton.alpha - 1.0)))
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if self.playButton.alpha == 1 {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.playButton.alpha = 0
                    })
                }
            }
        })
    }
    
    func photoPreviewControllerDismissed(controller: PhotoPreviewController) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    fileprivate func setUpPlayer(url: URL) {
        
        player = AVPlayer(url: url)
        playerLayer = AVPlayerLayer(player: player)
        playerLayer?.frame = playerView.bounds
        playerLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.addSublayer(playerLayer!)
        player?.play()
        player?.isMuted = true
        playerView.bringSubviewToFront(controlsView)
        controlsView.isHidden = false
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player?.currentItem)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.playButton.alpha == 1 {
                UIView.animate(withDuration: 0.3, animations: {
                    self.playButton.alpha = 0
                })
            }
        }
    }
    
    @objc private func playerItemDidReachEnd(_ notification: Notification) {
        if let item = notification.object as? AVPlayerItem {
            item.seek(to: CMTime.zero)
            player?.play()
        }
    }
    
    func showPost(_ link: String) {
        
        self.middleView.backgroundColor = .white
        //self.cons_t_total.constant = -(screenHeight / 20)
        
        self.startAnimShareView()
        self.playerView.isHidden = false
        self.lbl_copyPaste.isHidden = true
        self.setPreview(false)
        previewMedia(text: link)
    }
    
    private func previewMedia(text: String) {
        self.caption = ""
        previewMedia(with: text) {[weak self] (url, isVideo, feedModelOwner, medias, caption) in
            
            if let url = url{
                if url == URL.init(string: "https://instagram/private"){
                    self?.showToastCenter("Private Account Cannot Download", duration: 1.5)
                    self?.urlTextField.text = ""
                    self?.hidePost()
                    return
                }
                self?.currentUrl = url
                self?.isVideo = isVideo
                self?.feedModelOwner = feedModelOwner
                
                
                if let caption = caption{
                    // here getting caption
                    self?.caption = caption
                    //self?.pasteboard.string = caption
                    //print("this is the caption==>", caption)
                }
                
                self?.medias = medias
                
                if isVideo {
                    self?.setUpPlayer(url: url)
                } else {
                    self?.playerView.sendSubviewToBack((self?.controlsView)!)
                    self?.playerLayer?.removeFromSuperlayer()
                    self?.setImage(with: url.absoluteString, imageView: self?.userPhotoImageView)
                }
            }else{
                self?.hidePost()
            }
        }
    }
    
    func hidePost() {
        
       self.cons_t_total.constant = -(screenHeight / 20)
       UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {
           
           self.view.layoutIfNeeded()
         }, completion: { finished in
           print("Basket doors opened!")
         })
        
        setEdtPlaceholder(urlTextField, placeholderText: "Paste link here", placeColor: UIColor.lightGray, padding: .left(40))
        
        self.middleView.backgroundColor = .clear
        self.playerView.isHidden = true
        self.playerLayer?.removeFromSuperlayer()
        self.lbl_copyPaste.isHidden = false
        self.setPreview(true)
        currentUrl = nil
    }
}

// MARK: - UITextViewDelegate
extension DownloadController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let initialUrl = textField.text!
        if initialUrl.contains("instagram.com/"){
            if initialUrl.contains("stories"){
                self.downloadcase = downloadCase.story
                self.pasteboard.string = ""
                urlTextField?.text = initialUrl
                self.getstories(String(initialUrl.split(separator: "/")[3]))
                
            }else{
                urlTextField?.text = initialUrl
                self.showPost(initialUrl)
            }
        }else{
            self.hidePost()
        }
        
        
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let initialUrl = textField.text!
        if initialUrl.contains("instagram.com/"){
            if initialUrl.contains("stories"){
                self.downloadcase = downloadCase.story
                self.pasteboard.string = ""
                urlTextField?.text = initialUrl
                self.getstories(String(initialUrl.split(separator: "/")[3]))
                
            }else{
                urlTextField?.text = initialUrl
                self.showPost(initialUrl)
            }
        }else{
            self.hidePost()
        }
    }
    
    func gotoWebViewWithProgressBar(_ link: String)  {
        /*let navigationController = KAWebBrowser.navigationControllerWithBrowser()
        show(navigationController, sender: nil)
        navigationController.webBrowser()?.loadURLString(link)*/
        let toVC = self.storyboard?.instantiateViewController(withIdentifier: "AppStore")
        
        toVC?.modalPresentationStyle = .popover
        
        //self.navigationController?.pushViewController(toVC!, animated: true)
        self.present(toVC!, animated: true, completion: nil)
    }
    
    private func setImage(with path: String, imageView: UIImageView?) {
        if let url = URL(string: path) {
            imageView?.af_setImage(
                withURL: url,
                imageTransition: .crossDissolve(0.5),
                completion: { response in
                    imageView?.contentMode = .scaleAspectFill
                }
            )
        }
    }
    
    func showAlert()  {
        
        if !UserDefault.getBool(key: "rating", defaultValue: false){
            let refreshAlert = UIAlertController(title: "Rate This App \n⭐⭐⭐⭐⭐", message: nil, preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
              print("Handle Ok logic here")
              // goto Appstore link
                UserDefault.setBool(key: "rating", value: true)
                //self.gotoWebViewWithProgressBar(CONST.appStoreLink)
                self.writeReview()
              }))

            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
              //print("Handle Cancel Logic here")
              }))

            present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func showNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    private func writeReview() {
        var components = URLComponents(url: URL(string:CONST.appStoreLink)!, resolvingAgainstBaseURL: false)
      components?.queryItems = [
        URLQueryItem(name: "action", value: "write-review")
      ]

      guard let writeReviewURL = components?.url else {
        return
      }

      UIApplication.shared.open(writeReviewURL)
    }
}



extension DownloadController{
    func addBannerView(toView view: UIView, bannerViewDelegate: GADBannerViewDelegate, rootController: UIViewController) -> GADBannerView {
//        let bannerView = RMAppDelegate.adBannerView
        let bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        //bannerView.adUnitID = Config.Admob_ID
        bannerView.adUnitID = CONST.bannerID
        view.addSubview(bannerView)
        // add constraints
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addConstraints(
           [NSLayoutConstraint(item: bannerView,
                               attribute: .bottom,
                               relatedBy: .equal,
                               toItem: view,
                               attribute: .bottom,
                               multiplier: 1,
                               constant: 0),
            NSLayoutConstraint(item: bannerView,
                               attribute: .centerX,
                               relatedBy: .equal,
                               toItem: view,
                               attribute: .centerX,
                               multiplier: 1,
                               constant: 0)
           ])
        
        // load request
        bannerView.load(GADRequest())
        
        bannerView.delegate = bannerViewDelegate
        bannerView.rootViewController = rootController
        
        return bannerView
    }
}

enum downloadCase {
    case story
    case general
}


extension DownloadController: GADBannerViewDelegate {
    /// Tells the delegate an ad request loaded an ad.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("adViewDidReceiveAd")
        /*if addBottomLayout.constant < 50 {     // default banner height
            UIView.animate(withDuration: 1.0) {
                self.addBottomLayout.constant += 50
                self.clvBooks.contentInset.bottom += 50
                self.view.layoutIfNeeded()
            }
        }*/
    }
    
    /// Tells the delegate an ad request failed.
    func adView(_ bannerView: GADBannerView,
        didFailToReceiveAdWithError error: GADRequestError) {
      print("adView:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that a full-screen view will be presented in response
    /// to the user clicking on an ad.
    func adViewWillPresentScreen(_ bannerView: GADBannerView) {
      print("adViewWillPresentScreen")
    }
    
    /// Tells the delegate that the full-screen view will be dismissed.
    func adViewWillDismissScreen(_ bannerView: GADBannerView) {
      print("adViewWillDismissScreen")
    }
    
    /// Tells the delegate that the full-screen view has been dismissed.
    func adViewDidDismissScreen(_ bannerView: GADBannerView) {
      print("adViewDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app (such as
    /// the App Store), backgrounding the current app.
    func adViewWillLeaveApplication(_ bannerView: GADBannerView) {
      print("adViewWillLeaveApplication")
    }
}

extension DownloadController: GADInterstitialDelegate{
    // MARK: - GADInterstitialDelegate methods
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        //showInterstatialAd()
        
        /*
         Don't use the interstitialDidReceiveAd event to show the interstitial.
         This can cause a poor user experience. Instead, pre-load the ad before you need to show it. Then check the isReady method on GADInterstitial to find out if it is ready to be shown
         */
        
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
        
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
        //self.showToast("Rate This App!")
        let num = UserDefault.getInt(key: "count",defaultValue: 0)
        print(num)
        if num > 5{
            self.showAlert()
        }
        self.cons_t_total.constant = -(screenHeight / 20)
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut, animations: {

          self.view.layoutIfNeeded()
        }, completion: { finished in
          print("Basket doors opened!")
        })

        self.downloadcase = downloadCase.general
        if let initialUrl = urlTextField.text {
            if initialUrl.contains("instagram.com/"){
                if initialUrl.contains("stories"){
                    self.pasteboard.string = ""
                    self.downloadcase = downloadCase.story
                    urlTextField?.text = initialUrl
                    self.getstories(String(initialUrl.split(separator: "/")[3]))
                }else{
                    urlTextField?.text = initialUrl
                    self.showPost(initialUrl)
                }
            }else{
                self.hidePost()
            }
        }else{
            self.hidePost()
        }
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}


