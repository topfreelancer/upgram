//
//  AppStore.swift
//  Upgram
//
//  Created by top Dev on 10/2/20.
//  Copyright © 2020 top Dev. All rights reserved.
//

import UIKit
import WebKit

class AppStore: UIViewController {

    @IBOutlet weak var wbv_link: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let link = URL(string:CONST.appStoreLink)!
        let request = URLRequest(url: link)
        wbv_link.load(request)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavBar()
    }
    
    func showNavBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func hideNavBar() {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
