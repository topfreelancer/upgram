//
//  Constants.swift
//  Upgram
//
//  Created by top Dev on 10/2/20.
//  Copyright © 2020 top Dev. All rights reserved.
//

import Foundation
struct CONST {
    public static let initialID = "ca-app-pub-4758294084210006/6813867978"
    public static let bannerID = "ca-app-pub-4758294084210006/5500006059"
    public static let testBannerID = "ca-app-pub-3940256099942544/6300978111"
    public static let testInitialID = "ca-app-pub-3940256099942544/1033173712"
    public static let appStoreLink = "https://itunes.apple.com/app/id1531605052"
}
