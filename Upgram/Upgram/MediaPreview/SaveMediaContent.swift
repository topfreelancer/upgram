import Foundation
import Photos


struct SaveMediaContent {
    static func saveImage(_ image: UIImage?) {
        checkPermisions {
            if let image = image {
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }, completionHandler: { (saved, error) in
                    if saved {
                        //Alert.show(title: nil, message: "Photo was successfully saved.")
                        NotificationCenter.default.post(name: .showToast, object: nil)
                    }
                })
            }
        }
    }
    
    /*static func saveImages(_ image: [UIImage]?) {
        checkPermisions {
            if let images = image {
                var num = 0
                for one in images{
                    num += 1
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAsset(from: one)
                    }, completionHandler: { (saved, error) in
                        if saved {
                           
                        }
                    })
                    if num == images.count{
                         Alert.show(title: nil, message: "Photos was successfully saved.")
                    }
                }
            }
        }
    }*/
    
    static func saveVideo(_ path: URL) {
        checkPermisions {
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: path)
            }) { saved, error in
                if saved {
                    try? FileManager.default.removeItem(at: path)
                    //Alert.show(title: nil, message: "Video was successfully saved.")
                    NotificationCenter.default.post(name: .showToast, object: nil)
                }
            }
        }
    }
    
    static func saveImagewithFlag(_ image: UIImage?, completionHandler: ((_ isDownloaded: Bool) -> Void)?) {
        
        checkPermisions {
            if let image = image {
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAsset(from: image)
                }, completionHandler: { (saved, error) in
                    if saved {
                        completionHandler?(true)
                    }else{
                        completionHandler?(false)
                    }
                })
            }
        }
    }
    
    static func saveVideowithFlag(_ path: URL, completionHandler: ((_ isDownloaded: Bool) -> Void)?) {
        checkPermisions {
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: path)
            }) { saved, error in
                if saved {
                    try? FileManager.default.removeItem(at: path)
                    completionHandler?(true)
                }else{
                    completionHandler?(false)
                }
            }
        }
    }
    
    /*static func saveVideos(_ path: [URL]) {
        checkPermisions {
            var num = 0
            for one in path{
                num += 1
                PHPhotoLibrary.shared().performChanges({
                    PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: one)
                }) { saved, error in
                    if saved {
                        try? FileManager.default.removeItem(at: one)
                        
                    }
                }
                if num == path.count{
                    Alert.show(title: nil, message: "Videos was successfully saved.")
                }
            }
        }
    }*/
    
    static private func checkPermisions(completionHandler: (() -> Void)?) {
        let status = PHPhotoLibrary.authorizationStatus()
        if status == .authorized {
            completionHandler?()
        } else if status == .denied {
            Alert.show(title: nil, message: "Please allow access.", okAction: {
                UIApplication.shared.openURL(NSURL(string: UIApplication.openSettingsURLString)! as URL)
            }, cancelAction: {
                
            })
        } else {
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == .authorized {
                    completionHandler?()
                }
            })
        }
    }
    
    private init(){}
    
}
