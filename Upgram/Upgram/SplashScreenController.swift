import UIKit
import GoogleMobileAds
import SwiftyUserDefaults

final class SplashScreenController: UIViewController, StoryboardInstantiable {
    
    static var storyboardName: String = SplashScreenController.identifier
    var window: UIWindow?
    var interstitial: GADInterstitial?
    let appDel: AppDelegate = (UIApplication.shared.delegate as? AppDelegate)!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.setStatusBarAndInitialAdvertisement()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.gotoDownloadVC()
        }
        //NotificationCenter.default.addObserver(self, selector: #selector(showAd),name:.showAd, object: nil)
        let num = UserDefault.getInt(key: "count",defaultValue: 0)
        let count = num + 1
        UserDefault.setInt(key: "count", value: count)
        if num % 5 == 0{
            //self.showInterstatialAd()
        }
        
    }
    
    @objc func showAd(){
        self.showInterstatialAd()
    }
    
    func gotoDownloadVC() {
        //self.window?.rootViewController = DownloadController.instantiate()
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: "DownloadController")
       
        UIApplication.shared.keyWindow?.rootViewController = targetVC
        //targetVC.modalPresentationStyle = .fullScreen
        
        //self.present(targetVC, animated: false, completion: nil)
    }
    
    func setStatusBarAndInitialAdvertisement()  {
        //let statusBar: UIView = UIApplication.shared.statusBarUIView!
        
        //statusBar.backgroundColor = UIColor.init(red: 23/255, green: 23/255, blue: 23/255, alpha: 1)

        // Do any additional setup after loading the view.
        UIApplication.shared.isStatusBarHidden=true
        interstitial = createAndLoadInterstitial()
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        //interstitial = GADInterstitial(adUnitID: ADMOB_INTERTITIALID)
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/1033173712")
        interstitial?.delegate = self
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID as! String]
        interstitial?.load(request)
       
        return interstitial!
    }
    
    func showInterstatialAd(){
        if (self.interstitial?.isReady)! {
            self.interstitial?.present(fromRootViewController: self)
        } else {
            print("Ad wasn't ready")
            //self.performSegue(withIdentifier: "sugueToNewsVC", sender: nil)
        }
    }
    
}

extension SplashScreenController: GADInterstitialDelegate{
    // MARK: - GADInterstitialDelegate methods
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
        print("interstitialDidReceiveAd")
        showInterstatialAd()
        
        /*
         Don't use the interstitialDidReceiveAd event to show the interstitial.
         This can cause a poor user experience. Instead, pre-load the ad before you need to show it. Then check the isReady method on GADInterstitial to find out if it is ready to be shown
         */
        
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
        print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
        
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
        print("interstitialWillPresentScreen")
    }
    
    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        print("interstitialWillDismissScreen")
        self.gotoDownloadVC()
        
    }
    
    /// Tells the delegate the interstitial had been animated off the screen.
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        print("interstitialDidDismissScreen")
    }
    
    /// Tells the delegate that a user click will open another app
    /// (such as the App Store), backgrounding the current app.
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        print("interstitialWillLeaveApplication")
    }
}

extension UIApplication {
var statusBarUIView: UIView? {
    if #available(iOS 13.0, *) {
        let tag = 38482
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

        if let statusBar = keyWindow?.viewWithTag(tag) {
            return statusBar
        } else {
            guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
            let statusBarView = UIView(frame: statusBarFrame)
            statusBarView.tag = tag
            keyWindow?.addSubview(statusBarView)
            return statusBarView
        }
    } else if responds(to: Selector(("statusBar"))) {
        return value(forKey: "statusBar") as? UIView
    } else {
        return nil
    }
  }
    
}
